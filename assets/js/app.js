var prijs_schema = {
    "1-10" : { "1-2":14.3, "3-5":13.3, "6-10":12.4, "11-25":12.4, "26-50":11.4, "51-75":9.6, "76-100":7.1, "101+":4.7 },
    "11-25" : { "1-2":11.9, "3-5":11.5, "6-10":11.5, "11-25":9.6, "26-50":7.1, "51-75":4.7, "76-100":4.7, "101+":4.1 },
    "26-50" : { "1-2":10.0, "3-5":5.4, "6-10":4.7, "11-25":4.7, "26-50":4.3, "51-75":3.8, "76-100":3.4, "101+":3.0 },
    "51-100" : { "1-2":7.2, "3-5":4.1, "6-10":4.0, "11-25":3.0, "26-50":3.6, "51-75":3.3, "76-100":3.0, "101+":2.7 },
    "101-250" : { "1-2":3.6, "3-5":3.7, "6-10":3.6, "11-25":3.3, "26-50":3.1, "51-75":2.9, "76-100":2.5, "101+":2.4 },
    "251-500" : { "1-2":3.1, "3-5":3.1, "6-10":3.1, "11-25":2.9, "26-50":2.9, "51-75":2.6, "76-100":2.4, "101+":2.1 },
    "501-750" : { "1-2":2.6, "3-5":2.6, "6-10":2.6, "11-25":2.6, "26-50":2.6, "51-75":2.4, "76-100":2.1, "101+":1.8 },
    "751-1000" : { "1-2":2.5, "3-5":2.4, "6-10":2.4, "11-25":2.4, "26-50":2.3, "51-75":2.1, "76-100":1.8, "101+":1.8 },
    "1001-2500" : { "1-2":2.1, "3-5":2.1, "6-10":2.1, "11-25":2.1, "26-50":2.1, "51-75":1.8, "76-100":1.8, "101+":1.8 },
    "2501-5000" : { "1-2":2.0, "3-5":2.0, "6-10":2.0, "11-25":1.9, "26-50":1.9, "51-75":1.8, "76-100":1.8, "101+":1.8 },
    "5000+" : { "1-2":2.0, "3-5":1.9, "6-10":1.9, "11-25":1.8, "26-50":1.8, "51-75":1.8, "76-100":1.8, "101+":1.7 }
};

var input_data = [];

// engine
var opdracht_row = null;

$( document ).ready(function() {
    
    setBox();
    
    opdracht_row = $(".opdracht-row").outerHTML();

    $( "#nieuwe-opdracht" ).click(function() {
        $("#opdrachten-form").append(opdracht_row);        
    });
    
    $('body').on('click', '#reset-opdrachten', function() {
        $("#opdrachten-form").html(opdracht_row);     
        resetOpdrachten();
    });
    
    $('body').on('click', '.verwijder-opdracht', function() {
        
        if($('.opdracht-row').length <= 1) {
            $("#opdrachten-form").html(opdracht_row);     
            resetOpdrachten();
            $("#totaal_kopieen").text('0');
            $("#totaal_prijs_exclbtw").text('0,00');
            return;
        }
        
        var element = $(this).parent().parent().parent();
        element.remove();
        resetOpdrachten();
    });
    
    $('body').on('change', '.input_orginelen', function() {
        resetOpdrachten();
    });
    
    $('body').on('change', '.input_kopieen', function() {
        resetOpdrachten();
    });
    
    $(document).on('change', ".input_formaat", function () {
        resetOpdrachten();
    });
    
    $('body').on('focusout', '.input_kopieen', function() {
        resetOpdrachten();
    });
    
    $( window ).resize(function() {
       setBox(); 
    });
    
    $('body').on('change', '.input_kopieen', function() {
        var value = $(this).val();
        var new_value = value.replace(/[^0-9]/g, "");
        $(this).val(new_value);
    });

});

// functie zodat gehele element wordt geselecteerd.
jQuery.fn.outerHTML = function() 
{
    return jQuery('<div />').append(this.eq(0).clone()).html();
}

function resetOpdrachten() 
{
    input_data = [];   
    
    $( ".opdracht-row" ).each(function( index, element )
    {
        var orginelen = $(element).find(".input_orginelen");
        var kopieen = $(element).find(".input_kopieen");
        
        if(orginelen.val() != 0 && kopieen.val() == 0) {
            kopieen.focus();
        }
        
        if(orginelen.val() == 0 && kopieen.val() != 0) {
            orginelen.val('1');
        }
        
        if(orginelen.val() != 0 && kopieen.val() != 0) {
            input_data.push([
                parseInt(orginelen.val()), 
                parseInt(kopieen.val()), 
                1
            ]); 
        }
        
    });
    
    calcOpdrachten();
}

function calcOpdrachten() {
    var num_of_copies = 0;
    var prijs_excl = 0;
 
    if(input_data == undefined || input_data.length == 0) {
        $("#totaal_kopieen").text('NaN');
        $("#totaal_prijs_exclbtw").text('NaN');
        return;
    }

    for(var i = 0; i < input_data.length; i++) {
        num_of_copies += ((input_data[i][0] * input_data[i][1]) * input_data[i][2]);
    }
    $("#totaal_kopieen").text(num_of_copies);
    
    for(var o = 0; o < input_data.length; o++) {
        for (var i in prijs_schema) {
            for (var j in prijs_schema[i]) {

                if(isBetween(input_data[o][0], j) && isBetween(input_data[o][1], i)) {
                    prijs_excl += ((input_data[o][0] * input_data[o][1]) * (prijs_schema[i][j]) * input_data[o][2]);
                    var opdrachten = $(".opdracht-row").eq(o);
                    opdrachten.find(".amount").eq(0).find("strong").text((input_data[o][0] * input_data[o][1]));
                    var prijsps = (prijs_schema[i][j] / 100).toFixed(3);
                    opdrachten.find(".amount").eq(1).find("strong").text(prijsps);
                }

            }
        }
    }
    var price = String(Math.round(prijs_excl).toFixed(2) / 100);
    price = price.replace(".", ",");
    $("#totaal_prijs_exclbtw").text(price);
    
}

function isBetween(toCheck, StringCheck) 
{
    
    if(StringCheck.endsWith("+")) {
        var number = StringCheck.split('\+');
        if(toCheck >= number[0])
            return true;
        else
            return false;
    } else {
        var number = StringCheck.split('-');
        
        if (toCheck >= number[0] && toCheck <= number[1])
            return true;
        else
            return false;
    }
    
    return false;
}

function setBox() {
    
    var height = $("#MainContainer").height();
    var window_height = window.innerHeight;
    
    var new_height = (window_height / 2) - (height / 2);
    
    if(window_height > (height + 150))
         $('#MainContainer').css('margin-top', (new_height - (window_height/8)));
    else
        $('#MainContainer').css('margin-top', 5);

}